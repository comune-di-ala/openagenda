<?php /* #?ini charset="utf-8"?

[ClassSettings]
#ClassConnectors[article]=MyAwesomeArticleClassConnector
#ClassGroupConnectors[1]=MyAwesomeContentClassConnector
ClassConnectors[image]=ImageClassConnector

[FieldSettings]
FieldConnectors[ezxmltext]=\Opencontent\Ocopendata\Forms\Connectors\OpendataConnector\FieldConnector\EzOnlineEditorXmlField
FieldConnectors[ezurl]=UrlFieldConnector
FieldConnectors[eztags]=TreeTagsFieldConnector
FieldConnectors[has_street_toponym]=SelectTagsFieldConnector
FieldConnectors[city]=SelectTagsFieldConnector
FieldConnectors[province]=SelectTagsFieldConnector
FieldConnectors[region]=SelectTagsFieldConnector
FieldConnectors[country]=SelectTagsFieldConnector
FieldConnectors[time_indexed_role/role]=SelectTagsFieldConnector
FieldConnectors[has_temporal_coverage]=FullRelationsField
FieldConnectors[opening_hours_specification]=FullRelationsField
FieldConnectors[opening_hours]=OpeningHoursMatrixField
*/ ?>